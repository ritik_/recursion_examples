import java.util.*;
class Fact {
	int facto(int n)
	{
		if(n==0)
		{
			return 1;
		}
		else
		{
			return (n*facto(n-1));
		}
	}
}
public class Factorial{
	public static void main(String[] args)
	{
		Scanner s = new Scanner (System.in);
		int num = s.nextInt();
		int result;
		
		Fact f = new Fact();
		result = f.facto(num);
		System.out.println("The Factorial of " + num + " is " + result);
	}
}
