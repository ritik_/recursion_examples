import java.util.*;

public class Fibonacci{
    public static void main(String args[]) {
    	Scanner s = new Scanner(System.in);
    	int num = s.nextInt();
    	Fibon f = new Fibon();
    	
    	System.out.print("Fibonacci Series of "+num+" number(s) is: ");
    	for(int i = 0; i < num; i++){
    		System.out.print(f.fibo(i) +" ");
		}
	}
}
class Fibon{
	int fibo(int n){
		if(n == 0){
			return 0;
		}
		if(n == 1 || n==2)
			return 1;
	    
			return fibo(n-2) + fibo(n-1);
	}
}